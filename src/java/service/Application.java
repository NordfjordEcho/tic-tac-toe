package service;

import display.Display;
import domain.Game;

/**
 * Created with IntelliJ IDEA.
 * User: gmatt
 * Date: 24.11.2012
 * Time: 13:19
 *
 * This is the central application class.
 * It handles inter-class communication
 * and controls method execution.
 */
public class Application {

    private Game game;

    private Display display;

    public Game getGame() {
        return game;
    }

    public Display getDisplay() {
        return display;
    }

    /**
     * on first run, initialize the application
     */
    public Application() {
        initialize();
    }

    /**
     * initializes new game and display objects
     * to start a new game
     */
    public void initialize() {
        game = new Game();
        display = new Display();
        display.setApp(this);
    }

    /**
     * changes the state of the game and display
     * objects in accordance with user input
     * @param index the index of the clicked button
     */
    public void changeState(int index) {
        game.changeState(index);
        display.showResult(index, game.getState(index));
        if ( game.checkForWin() ) {
            display.winResult(game.getPlayer());
            initialize();
        }
        if ( game.fullBoard() ) {
            display.winResult(null);
            initialize();
        }
    }

    /**
     * initializes the central application object
     * for first run
     * @param args system arguments
     */
    public static void main ( String[] args ) {
        new Application();
    }

}
