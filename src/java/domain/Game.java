package domain;
/**
 * Created with IntelliJ IDEA.
 * User: Krilli
 * Date: 25.11.2012
 * Time: 13:15
 *
 * This object represents the active game
 * and contains the business logic behind
 * tic-tac-toe. It handles real-time data
 * about the current state of each square.
 */
public class Game {

    /**
     * array of all possible solutions
     * by index
     */
    private final int[][] solutions = {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, // horizontal solutions
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},  // vertical solutions
            {0, 4, 8}, {2, 4, 6}             // diagonal solutions
    };

    String[] state = new String[9];
    int counter = 0;

    public Game()
    {
        for (int i = 0; i < 9; i++)
        {
            state[i] = "";
        }
    }

    public int getCount() {
        return counter;
    }

    /**
     * changes the effective state of the game
     * in accordance with user input
     * @param index the index of the clicked button
     */
    public void changeState(int index)
    {
        counter++;
        state[index] = getPlayer();
    }

    /**
     * returns X if the round number is odd and
     * O if it is even
     * @return the player that made the last move
     */
    public String getPlayer()
    {
        if (counter%2 == 1)
            return "X";
        else
            return "O";
    }

    /**
     * returns the actual value of a given square
     * in order to effectively update the display
     * @param i the index of the square
     * @return the value of square
     */
    public String getState(int i)
    {
        return state[i];
    }

    /**
     * checks if either player has won the game
     * @return true if victory has been achieved,
     *         else return false
     */
    public boolean checkForWin()
    {
        for (int[] solution : solutions)
            if (state[solution[0]].equals(state[solution[1]])
                    && state[solution[1]].equals(state[solution[2]])
                    && !(state[solution[0]].equals("")))
                return true;
        return false;
    }

    /**
     * checks if every square has been filled out
     * @return true if the board is full,
     *         else return false
     */
    public boolean fullBoard()
    {
        for (String aState : state)
        {
            if (aState.equals(""))
                return false;
        }
        return true;
    }
}
