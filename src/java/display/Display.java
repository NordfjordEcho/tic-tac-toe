package display;

import service.Application;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: gmatt
 * Date: 24.11.2012
 * Time: 17:08
 *
 * This is the graphical front-end of the application.
 * It handles only the graphical representation of game
 * data, and the passing of user commands to the
 * application.
 */
public class Display extends JFrame implements ActionListener, KeyListener{

    private JButton[] buttons;

    private Application app;

    public void setApp(Application app) {
        this.app = app;
    }

    /**
     * initializes the GUI
     */
    public Display() {
        super("TicTacToe");
        this.setSize(300, 300);
        buttons = new JButton[9];
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(3, 3));
        for ( int i = 0; i < 9; i++ ) {
            buttons[i] = new JButton("");
            this.add(buttons[i]);
            buttons[i].addActionListener(this);
            buttons[i].addKeyListener(this);
            buttons[i].setName(String.valueOf(i));
        }
        this.setVisible(true);
    }

    /**
     * changes the text on a button and disables
     * it, to prevent clicking the same button
     * twice
     * @param index the index of the button to change
     * @param value X or O, depending on the round number
     */
    public void showResult(int index, String value) {
        buttons[index].setText(value);
        buttons[index].setEnabled(false);
    }

    public JButton[] getButtons() {
        return buttons;
    }

    /**
     * shows the appropriate dialog in case of
     * victory or tie and disposes of itself
     * @param winner X, O or null, depending on
     *               the winner, and whether the
     *               board is full
     */
    public void winResult(String winner) {
        if (winner != null)
            JOptionPane.showMessageDialog(this, "Player " + winner + " wins!");
        else
            JOptionPane.showMessageDialog(this, "Tie!");
        this.dispose();
    }

    /**
     * forwards all button-press actions to the
     * application for processing
     * @param e the event of a user clicking a button
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton pressed = (JButton)e.getSource();
        int index = Integer.valueOf(pressed.getName());
        app.changeState(index);
    }

    /**
     * forwards numpad- and escape key press actions
     * to the application for processing
     * @param e the event of a user pressing
     *          a keyboard button
     */
    @Override
    public void keyPressed(KeyEvent e) {

        if ( e.getKeyCode() == 27 ) System.exit(0);
        int index = Integer.valueOf(e.getKeyChar());
        if ( index < 58 && index > 48 ) {
            index = Math.abs(index-57);

            if ( (index+1) % 3 == 0 ) index -= 2;
            else if ( (index+1) % 3 == 1 ) index += 2;

            if ( buttons[index].isEnabled() )
                app.changeState(index);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
