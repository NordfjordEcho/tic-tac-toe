package display;

import junit.framework.Assert;
import org.junit.Test;
import service.Application;

import javax.swing.*;
/**
 * @author Einar Sigurðsson
 * @version 1.0, 11/24/12
 */
public class DisplayTest {

    private Application app = new Application();

    @Test
    public void testShowResult() {
        app.getDisplay().showResult(0, "X");
        if ( app.getDisplay().getButtons()[0].getText() != "X" ) {
            Assert.fail();
        }
        if ( app.getDisplay().getButtons()[0].isEnabled()) {
            Assert.fail();
        }
        try {
            app.getDisplay().showResult(9, "O");
            Assert.fail();
        } catch ( ArrayIndexOutOfBoundsException e ) {}

    }


    @Test
    public void testActionPerformed() {
        JButton j = app.getDisplay().getButtons()[0];
        j.doClick();
        Assert.assertEquals("X", j.getText());
    }

}
