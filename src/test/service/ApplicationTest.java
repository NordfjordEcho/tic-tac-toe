package service;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: gmatt
 * Date: 25.11.2012
 * Time: 13:40
 */
public class ApplicationTest {

    private Application app = new Application();

    @Test
    public void testChangeStateOnGame(){
        app.changeState(0);
        Assert.assertEquals("X", app.getGame().getState(0));
        Assert.assertEquals(1, app.getGame().getCount());
    }

    @Test
    public void testChangeStateOnDisplay(){
        app.changeState(0);
        Assert.assertEquals("X", app.getDisplay().getButtons()[0].getText());
        Assert.assertFalse(app.getDisplay().getButtons()[0].isEnabled());
    }

}
