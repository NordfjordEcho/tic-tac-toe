package domain;

import junit.framework.Assert;
import org.junit.Test;

/**
 * @author Einar Sigurðsson
 * @version 1.0, 11/24/12
 */
public class GameTest {

    private Game game = new Game();

    // Test getPlayer

    @Test
    public void testCorrectPlayer(){
        // Count is incremented before every move
        // Player X has first move
        game.changeState(0);
        Assert.assertEquals("X", game.getPlayer());
        game.changeState(1);
        Assert.assertEquals("O", game.getPlayer());
    }

    // Test changeState

    @Test
    public void testCorrectMark(){
        game.changeState(1);
        Assert.assertEquals("X", game.getState(1));
    }

    // Test getCount

    @Test
    public void testCorrectCount(){
        game.changeState(0);
        game.changeState(1);
        game.changeState(2);
        game.changeState(3);
        Assert.assertEquals(4, game.getCount());
    }

    // Test FullBoard

    @Test
    public void testFalseFullBoard(){
        boolean full = game.fullBoard();
        Assert.assertFalse(full);
    }

    @Test
    public void testFullBoard(){
        game.changeState(0);
        boolean full = game.fullBoard();
        Assert.assertFalse(full);
        game.changeState(8);
        game.changeState(6);
        game.changeState(2);
        game.changeState(5);
        full = game.fullBoard();
        Assert.assertFalse(full);
        game.changeState(3);
        game.changeState(1);
        game.changeState(4);
        full = game.fullBoard();
        Assert.assertFalse(full);
        game.changeState(7);
        full = game.fullBoard();
        Assert.assertTrue(full);
    }

    // Test checkForWin()

    @Test
    public void testFalseWin(){
        game.changeState(0);
        game.changeState(5);
        game.changeState(1);
        game.changeState(8);
        boolean win = game.checkForWin();
        Assert.assertFalse(win);
    }

    @Test
    public void testWinConditionOne(){
        game.changeState(0);
        game.changeState(3);
        game.changeState(1);
        game.changeState(4);
        game.changeState(2);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionTwo(){
        game.changeState(3);
        game.changeState(0);
        game.changeState(4);
        game.changeState(2);
        game.changeState(5);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionThree(){
        game.changeState(6);
        game.changeState(0);
        game.changeState(7);
        game.changeState(2);
        game.changeState(8);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionFour(){
        game.changeState(0);
        game.changeState(1);
        game.changeState(3);
        game.changeState(2);
        game.changeState(6);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionFive(){
        game.changeState(1);
        game.changeState(0);
        game.changeState(4);
        game.changeState(2);
        game.changeState(7);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionSix(){
        game.changeState(2);
        game.changeState(0);
        game.changeState(5);
        game.changeState(1);
        game.changeState(8);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionSeven(){
        game.changeState(0);
        game.changeState(1);
        game.changeState(4);
        game.changeState(2);
        game.changeState(8);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

    @Test
    public void testWinConditionEight(){
        game.changeState(2);
        game.changeState(1);
        game.changeState(4);
        game.changeState(0);
        game.changeState(6);
        boolean win = game.checkForWin();
        Assert.assertTrue(win);
    }

}
